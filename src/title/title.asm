.proc title

	.proc init

		; disable rendering
		lda #%00000000
		sta PPU_MASK

		.proc load_map

			current_map			= $00fe
			column_one_written	= RAM+$02
			column_two_written	= RAM+$03

			bit PPU_STATUS
			lda #>PPU_NAMETABLE_0
			sta PPU_ADDRESS
			lda #<PPU_NAMETABLE_0
			sta PPU_ADDRESS

			lda #<maps::world_0_0
			sta current_map
			lda #>maps::world_0_0
			sta current_map+1
		
			load_row:

				ldy #0
				sty column_one_written
				sty column_two_written
				sty $01
		
			@column_one:
				ldy column_one_written
				ldx #0
			:
				lda (current_map),y
				jsr write_tile
				bne :-

				sty column_one_written
		
			@switch_to_next_map:
				lda current_map
				clc
				adc #80
				sta current_map
				bcc :+
				inc current_map+1
			:

			@column_two:
				ldy column_two_written
				ldx #0
			:
				lda (current_map),y
				jsr write_tile
				bne :-

				sty column_two_written
				cpy #80
				bne @switch_back
				jmp @next_row

			@switch_back:
				lda current_map
				clc
				;sbc #80
				adc #256-80		; FUCK IT, IT WORKS!
				sta current_map
				bcs :+
				dec current_map+1
			:
				jmp @column_one
		
			@next_row:
				lda current_map+1
				cmp #>maps::world_1_0
				bne :+
				lda current_map
				cmp #<maps::world_1_0
				bne :+
				lda #>maps::world_0_1
				sta current_map+1
				lda #<maps::world_0_1
				sta current_map
				jmp load_row
			:
				lda current_map+1
				cmp #>maps::world_1_1
				bne :+
				lda current_map
				cmp #<maps::world_1_1
				bne :+
				lda #>maps::world_0_2
				sta current_map+1
				lda #<maps::world_0_2
				sta current_map
				jmp load_row
			:
				jmp done

			.proc write_tile

				sta $00
				jsr get_tile_from_world_map
				lda $02
				sta PPU_DATA
				lda $03
				sta PPU_DATA
				inx
				iny
				cpx #8
				rts

			.endproc

			done:

		.endproc

		; set scroll, re-enable rendering, wait for a fram
		lda #$00
		sta PPU_SCROLL
		sta PPU_SCROLL

		lda #%00011110
		sta PPU_MASK
		jsr vblank_wait

	.endproc

	.proc animation_one

		process = RAM+$00

		.enum process
			init
			wiping
			finished
		.endenum

		init:
			lda #process::init
			sta process

		loop:
			jsr vblank_wait
			jsr update_water

			lda process

			cmp #process::init
			bne :+
			jsr sprite_wipe
			jmp loop
		:
			cmp #process::wiping
			bne :+
			jsr sprite_wipe
			jmp loop
		:
			cmp #process::finished
			bne :+
			jmp done
		:
			jmp loop
		
		.proc update_water

			rts

		.endproc
		
		; RAM starts at $0301
		.proc sprite_wipe

			; variables
			wipe_frame_counter		= RAM+$01
			nametable_position_lo	= RAM+$02
			nametable_position_hi	= RAM+$03
			tiles_filled			= RAM+$04

			; sprite address
			wipe_sprites			= SPRITE_PAGE+$00
			next_sprite				= 4

			; constants
			title_box_start_tile_x	= 8
			title_box_start_tile_y	= 8
			title_box_width			= 16	; in tiles
			title_box_height		= 8		; in tiles
			title_box_total_height	= title_box_height * 8

			sprite_start_x = title_box_start_tile_x * 8
			sprite_start_y = title_box_start_tile_y * 8 - 1

			wipe_sprite_index		= SPRITE_INDEX_WIPE
			tile_blank_index		= TILE_INDEX_BLANK

			init_nametable_position	= PPU_NAMETABLE_0 + (title_box_start_tile_x) + (title_box_start_tile_y * $20)

			init_sprite_flags		= %00000000

			ppu_control_value		= %10010100

			direct_process:
				lda process
				cmp #process::init
				bne :+
				jmp init
			:
				jmp cycle
			
			init:
				lda #process::wiping
				sta process
				lda #0
				sta wipe_frame_counter
				sta tiles_filled

				lda #>init_nametable_position
				sta nametable_position_hi
				lda #<init_nametable_position
				sta nametable_position_lo
			
			@init_sprites_y:
				ldx #0
				lda #sprite_start_y
				tay
			:
				tya
				sta wipe_sprites+SPRITE_BYTE_Y,x
				clc
				adc #8
				tay
				txa
				clc
				adc #next_sprite
				tax
				cpx #next_sprite * title_box_height
				bne :-
			
			@init_sprites_flags:
				ldx #0
			:
				lda #init_sprite_flags
				sta wipe_sprites+SPRITE_BYTE_FLAGS,x
				txa
				clc
				adc #next_sprite
				tax
				cpx #next_sprite * title_box_height
				bne :-

			cycle:

			@sprite_load:
				ldx #0
			:
				lda #wipe_sprite_index
				clc
				adc wipe_frame_counter
				sta wipe_sprites+SPRITE_BYTE_INDEX,x
				txa
				clc
				adc #next_sprite
				tax
				cpx #next_sprite * title_box_height
				bne :-

				ldx #0
			
				lda tiles_filled
				asl
				asl
				asl
				clc
				adc #sprite_start_x
				ldx #0
				tay

			:
				tya
				sta wipe_sprites+SPRITE_BYTE_X,x
				txa
				clc
				adc #next_sprite
				tax
				cpx #next_sprite * title_box_height
				bne :-

				jsr oam_load_sprites
				inc wipe_frame_counter
				lda wipe_frame_counter
				cmp #8
				bne :+
				jmp @tile_load
			:
				rts
			
			@tile_load:
				lda #ppu_control_value
				sta PPU_CTRL
				bit PPU_STATUS
				lda nametable_position_hi
				sta PPU_ADDRESS
				lda nametable_position_lo
				sta PPU_ADDRESS
				ldx #0
			:
				lda #tile_blank_index
				sta PPU_DATA
				inx
				cpx #title_box_height
				bne :-

				lda #0
				sta wipe_frame_counter
				inc nametable_position_lo
				bcs :+
				inc nametable_position_hi
			:
				inc tiles_filled
				lda tiles_filled
				cmp #title_box_width
				bne :+
				lda #process::finished
				sta process
			:
				lda #0
				sta PPU_SCROLL
				sta PPU_SCROLL
				rts

		.endproc

		done:

	.endproc

	loop:
		jmp loop

.endproc

; takes map tile and frame in $00 and $01 and returns 
; pattern table tiles for the map tiles in $03 and $04
.proc get_tile_from_world_map

		full_byte		= $00
		frame			= $01
		return_tile_1	= $02
		return_tile_2	= $03
		a_save			= $04
		x_save			= $05
		y_save			= $06

		sta a_save
		stx x_save
		sty y_save

	tile1:
		lda full_byte
		and #$f0
		clc
		lsr
		lsr
		lsr
		lsr
		jsr translate
		sta return_tile_1

	tile2:
		lda full_byte
		and #$0f
		jsr translate
		sta return_tile_2

	done:
		lda a_save
		ldx x_save
		ldy y_save
		rts

	translate:
		cmp #WORLD_TILE_BLANK
		bne :+
		lda #TILE_INDEX_BLANK
		rts
	:
		cmp #WORLD_TILE_WATER
		bne :+
		lda #TILE_INDEX_WATER
		clc
		adc frame
		rts
	:
		rts

.endproc

; takes in current nametable number (y), row (a), and starting column (x)
; and sets the ppu address to write to the next line
.proc nametable_next_line

		nametable_number			= $00
		current_row					= $01
		starting_column				= $02
		nametable_base_address		= $03 ; $04
		new_address					= $05 ; $06

		sty nametable_number
		sta current_row
		stx starting_column

		jsr get_base_nametable

	get_new_address:
		lda current_row
		ldx #0

	@doubling_loop:
		clc
		asl
		bcc :+
		inc >new_address
	:
		inx
		cpx #4
		bne @doubling_loop
		ldx #0

	@adding_loop:
		clc
		adc starting_column
		bcc :+
		inc >new_address
	:
		inx
		cpx #4
		bne @adding_loop
		sta <new_address

	@add_base_to_new:
		clc
		lda <new_address
		adc <nametable_base_address
		lda >new_address
		adc >nametable_base_address

	set_address:
		bit PPU_STATUS
		lda >new_address
		sta PPU_ADDRESS
		lda <new_address
		sta PPU_ADDRESS
		rts

	get_base_nametable:
		cpy #0
		bne :+
		lda #>PPU_NAMETABLE_1
		sta >nametable_base_address
		lda #<PPU_NAMETABLE_1
		sta <nametable_base_address
		rts
	:
		cpy #1
		bne :+
		lda #>PPU_NAMETABLE_1
		sta >nametable_base_address
		lda #<PPU_NAMETABLE_1
		sta <nametable_base_address
		rts
	:
		cpy #2
		bne :+
		lda #>PPU_NAMETABLE_2
		sta >nametable_base_address
		lda #<PPU_NAMETABLE_2
		sta <nametable_base_address
		rts
	:
		cpy #3
		bne :+
		lda #>PPU_NAMETABLE_3
		sta >nametable_base_address
		lda #<PPU_NAMETABLE_3
		sta <nametable_base_address
		rts
	:
		rts

.endproc