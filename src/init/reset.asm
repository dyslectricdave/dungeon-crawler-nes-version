
init_reset:
; for naming convention

reset:

; disable interrupts and clear decimal
	sei
	cld

; disable_apu_irq
	ldx #%01000000
	stx APU_FRAME_COUNTER

; initiallize stack
	ldx #$ff
	txs

; initiallize ppu
	inx
	stx PPU_CTRL
	stx PPU_MASK

; initiallize apu
	stx APU_DMC_FLAGS

; wait for vblank
	jsr vblank_wait

clear_memory:

	txa

	@loop:
		sta ZEROPAGE,x
		sta STACK,x
		sta $0300,x
		sta $0400,x
		sta $0500,x
		sta $0600,x
		sta $0700,x
		sta $0800,x

		inx

		bne @loop

	jsr vblank_wait
