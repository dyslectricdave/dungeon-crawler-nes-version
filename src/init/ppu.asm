init_ppu:

; load init palette
	bit PPU_STATUS
	ldx #$00
	lda #>PPU_PALETTE_BG_UNIVERSAL
	sta PPU_ADDRESS
	lda #<PPU_PALETTE_BG_UNIVERSAL
	sta PPU_ADDRESS

	:
		lda palette_init,x
		sta PPU_DATA
		inx
		cpx #$20
		bne :-

; load init nametable
	ldx #$00
	ldy #$00
	bit PPU_STATUS
	lda #>PPU_NAMETABLE_0
	sta PPU_ADDRESS
	lda #<PPU_NAMETABLE_0
	sta PPU_ADDRESS
	lda #$20

	:
		sta PPU_DATA
		inx
		cpx #NAMETABLE_WIDTH
		bne :-
		ldx #$00
		iny
		cpy #NAMETABLE_HEIGHT
		bne :-

; set ppu scroll
	lda #$00
	sta PPU_SCROLL
	sta PPU_SCROLL

; initiallize sprite page
	lda #$ff
	ldx #$00

	:
		sta SPRITE_PAGE,x
		inx
		bne :-
	
; load sprites into oam
	jsr oam_load_sprites

; turn on screen drawing
	lda #%10010000
	sta PPU_CTRL
	lda #%00011110
	sta PPU_MASK

; jump to title (just in case)
	jmp title
