.segment "HEADER"
	.byte "NES",$1a	; file identifier
	.byte $02		; prg-rom size
	.byte $01		; chr-rom size
	.byte %00000000	; flags 6
	.byte %00000000 ; flags 7
	.byte $00		; mapper/submapper
	.byte $00		; prg-rom/chr-rom size MSB
	.byte $00		; prg-ram/eeprom size
	.byte $00		; chr-ram size
	.byte $00		; cpu/ppu timing
	.byte $00		; vs. system type / extended console type
	.byte $00		; miscellanious ROMs
	.byte $00		; default expansion device
