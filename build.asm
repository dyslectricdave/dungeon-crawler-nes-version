.segment "STARTUP"

	; Libraries
	.include "./include/nes.asm"

	; Definitions
	.include "./include/definitions.asm"

	; Shared functions
	.include "./include/procs.asm"

	; Data
	.include "./data/data.asm"

	; Code
	.include "./src/init/reset.asm"
	.include "./src/init/ppu.asm"
	.include "./src/macrostates.asm"
	.include "./src/interrupts/nmi.asm"
	.include "./src/interrupts/irq.asm"

.segment "CHARS"
	.incbin "./chr/chars.chr"

.segment "HEADER"
	.include "./src/header.asm"

.segment "VECTORS"
	.include "./src/vectors.asm"