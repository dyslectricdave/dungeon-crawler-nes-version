; init
	.include "./init/palettes.asm"

.scope maps

	world_0_0:
		.incbin "./maps/world/worldmap0x0.csv.bin"
	world_1_0:
		.incbin "./maps/world/worldmap1x0.csv.bin"
	world_0_1:
		.incbin "./maps/world/worldmap0x1.csv.bin"
	world_1_1:
		.incbin "./maps/world/worldmap1x1.csv.bin"
	world_0_2:
		.incbin "./maps/world/worldmap0x2.csv.bin"
	world_1_2:
		.incbin "./maps/world/worldmap1x2.csv.bin"
	
.endscope

	
; text

	text_title_screen:
		.incbin "./text/title_screen.txt"
		.byte $00
	