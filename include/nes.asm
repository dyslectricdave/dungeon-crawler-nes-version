; Memory
.define ZEROPAGE						$00
.define STACK							$0100

; PPU
.define PPU_CTRL						$2000
.define PPU_MASK						$2001
.define PPU_STATUS						$2002
.define PPU_SCROLL						$2005
.define PPU_ADDRESS						$2006
.define PPU_DATA						$2007

.define OAM_ADDR						$2003
.define OAM_DATA						$2004
.define OAM_DMA							$4014

; ram page for sprite data
.define SPRITE_PAGE						$0200

; sprite bytes
.define SPRITE_BYTE_Y			$00
.define SPRITE_BYTE_INDEX		$01
.define SPRITE_BYTE_FLAGS		$02
.define SPRITE_BYTE_X			$03

.define PPU_PALETTE_BG_UNIVERSAL		$3f00
.define PPU_PALETTE_BG_0				$3f01
.define PPU_PALETTE_BG_1				$3f05
.define PPU_PALETTE_BG_2				$3f09
.define PPU_PALETTE_BG_3				$3f0d
.define PPU_PALETTE_SPRITE_0			$3f11
.define PPU_PALETTE_SPRITE_1			$3f15
.define PPU_PALETTE_SPRITE_2			$3f19
.define PPU_PALETTE_SPRITE_3			$3f1d

.define PPU_NAMETABLE_0					$2000
.define PPU_NAMETABLE_1					$2400
.define PPU_NAMETABLE_2					$2800
.define PPU_NAMETABLE_3					$2c00

.define PPU_ATTRIBUTE_TABLE_0			$23c0
.define PPU_ATTRIBUTE_TABLE_1			$27c0
.define PPU_ATTRIBUTE_TABLE_2			$2bc0
.define PPU_ATTRIBUTE_TABLE_3			$2fc0

; APU
.define APU_PULSE1_DUTY					$4000
.define APU_PULSE1_SWEEP				$4001
.define APU_PULSE1_LOW					$4002
.define APU_PULSE1_HIGH					$4003

.define APU_PULSE2_DUTY					$4004
.define APU_PULSE2_SWEEP				$4005
.define APU_PULSE2_LOW					$4006
.define APU_PULSE2_HIGH					$4007

.define APU_TRIANGLE_UNMUTE				$4008
.define APU_TRIANGLE_LOW				$400a
.define APU_TRIANGLE_HIGH				$400b

.define APU_NOISE_VOLUME				$400c
.define APU_NOISE_TONE_PERIOD			$400e
.define APU_LENGTH_COUNTER				$400f

.define APU_DMC_FLAGS					$4010
.define APU_DMC_LOAD_COUNTER			$4011
.define APU_DMC_SAMPLE_ADDRESS			$4012
.define APU_DMC_SAMPLE_LENGTH			$4013
.define APU_DMC_CONTROL					$4015
.define APU_DMC_STATUS					$4015

.define APU_FRAME_COUNTER				$4017

;	Controller
.define CONTROLLER_POLL					$4016
.define CONTROLLER1						$4016
.define CONTROLLER2						$4017

;	Buttons
.define BUTTON_A					%10000000
.define BUTTON_B					%01000000
.define BUTTON_SELECT				%00100000
.define BUTTON_START				%00010000
.define BUTTON_UP					%00001000
.define BUTTON_DOWN					%00000100
.define BUTTON_LEFT					%00000010
.define BUTTON_RIGHT				%00000001
