; world map
.define WORLD_MAP_BYTES 	80
.define WORLD_MAPS_PER_ROW	2

.define WORLD_TILE_BLANK		$00
.define WORLD_TILE_GRASS		$01
.define WORLD_TILE_TREE			$02
.define WORLD_TILE_WATER		$03
.define WORLD_TILE_CASTLE_1		$04
.define WORLD_TILE_CASTLE_2		$05
.define WORLD_TILE_TOWN_1		$06
.define WORLD_TILE_TOWN_2		$07
.define WORLD_TILE_CAVE			$08
.define WORLD_TILE_LADDER		$09
.define WORLD_TILE_VER_BRIDGE	$0a
.define WORLD_TILE_HOR_BRIDGE	$0b
.define WORLD_TILE_MOUNT_LEFT	$0c
.define WORLD_TILE_MOUNT_MIDDLE	$0d
.define WORLD_TILE_MOUNT_RIGHT	$0e
.define WORLD_TILE_MOUNT_TOP	$0f

; tile indicies
.define TILE_INDEX_BLANK		$20
.define TILE_INDEX_WATER		$10 ; $10 - $17
.define TILE_INDEX_WIPE			$18 ; $18 - $1f

; sprite indicies
.define SPRITE_INDEX_WIPE	$f8

; nametable helpers for legibility
.define NAMETABLE_WIDTH		32
.define NAMETABLE_HEIGHT	30

; start of ram area
.define RAM					$0300

; temp ram places for subroutines
.define INPUT				ZEROPAGE+$00

.define true				1
.define false				0