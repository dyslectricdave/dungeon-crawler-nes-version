.proc vblank_wait

	bit PPU_STATUS
	bpl vblank_wait
	rts

.endproc

.proc oam_load_sprites

	lda #>SPRITE_PAGE
	sta OAM_DMA
	nop
	rts

.endproc

.proc poll_controller

	lda #$01
	sta CONTROLLER_POLL
	sta INPUT
	lsr a
	sta CONTROLLER_POLL
	
	:
		lda CONTROLLER1
		lsr a
		rol INPUT
		bcc :-
	
	rts

.endproc